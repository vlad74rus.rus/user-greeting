﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace UserGreeting
{
    class Program
    {
        private static string _name;
        private static DateTime _birthDate;
        private static int _years;
        private static string _nameRegexFormat = @"[a-zA-Z]+";

        static void Main(string[] args)
        {
            GreetUser();
            SetUserDataFromDialog();
            ShowMessageAfterDataEntry();
        }

        static void GreetUser()
        {
            Console.WriteLine("Привет!");
        }

        static void SetUserDataFromDialog()
        {
            SetName();
            SetBirthDate();
        }

        static void SetName()
        {
            string name;

            Console.WriteLine("Введите имя: ");
            name = Console.ReadLine();

            if (IsCorrectedName(name))
            {
                _name = name;
            }
            else
            {
                throw new InvalidCastException("The name is not in the correct format");
            }
        }

        static void SetBirthDate()
        {
            string birthDay, birthMonth, birthYear;

            Console.WriteLine("Введите день рождения: ");
            birthDay = Console.ReadLine();

            Console.WriteLine("Введите месяц рождения: ");
            birthMonth = Console.ReadLine();

            Console.WriteLine("Введите год рождения: ");
            birthYear = Console.ReadLine();

            if (IsCorrectedBirthDate(birthDay, birthMonth, birthYear))
            {
                _birthDate = new DateTime(ConvertNumberToInt(birthYear), 
                    ConvertNumberToInt(birthMonth), 
                    ConvertNumberToInt(birthDay));
                _years = CalculateYears();
            }
            else
            {
                throw new InvalidCastException("The birth date is not in the correct format");
            }
        }

        static void ShowMessageAfterDataEntry()
        {
            var message = $"Привет, {_name}, Ваш возраст равен {_years} лет. Приятно познакомиться";

            Console.WriteLine(message);
        }

        static int CalculateYears()
        {
            var timeSpan = DateTime.Now - _birthDate;
            var years = Convert.ToInt32(timeSpan.TotalDays / 365.25);

            return years;
        }

        static bool IsCorrectedName(string name)
        {
            var regex = new Regex(_nameRegexFormat);

            return regex.IsMatch(name);
        }

        static bool IsCorrectedBirthDate(string birthDay, string birthMonth, string birthYear)
        {
            var birthDate = $"{birthDay}/{birthMonth}/{birthYear}";

            bool isExistDate = DateTime.TryParseExact(birthDate, "dd/mm/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None ,out var dateValue);
            bool isBirthDateLessCurrentDate = dateValue < DateTime.Now;

            return isExistDate & isBirthDateLessCurrentDate;
        }

        static int ConvertNumberToInt(string number)
        {
            return Convert.ToInt32(number);
        }
    }
}
